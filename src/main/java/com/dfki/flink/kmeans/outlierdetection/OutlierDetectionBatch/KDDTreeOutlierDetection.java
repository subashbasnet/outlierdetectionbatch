package com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;

import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.Point;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.Tokenizer;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.getPoints;

public class KDDTreeOutlierDetection {

	public static void main(String args[]) {
		if (!parseParameters(args)) {
			return;
		}
		// set up execution environment
		ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		// get input data
		DataSet<String> data = getTextDataSet(env);
		DataSet<Tuple2<Integer, Double[]>> newDataSet = data.flatMap(new Tokenizer());
		DataSet<Point> points = newDataSet.map(new getPoints());
		DataSet<Tuple2<Point, Double>> pointsWithDistance = points.map(new findDistanceToNearestPoint(2))// the
																											// number
																											// is
																											// the
																											// dimension
																											// of
																											// data
				.withBroadcastSet(points, "points");
		DataSet<Double> distance = pointsWithDistance.map(new TuplePointToDistance());
		DataSet<Tuple2<Point, Boolean>> finalPoints = pointsWithDistance.map(new findOutliers())
				.withBroadcastSet(distance, "distance");
		;
		try {
			if (fileOutput) {
				finalPoints.writeAsCsv(outputPath, "\n", " ");
			} else {
				finalPoints.print();
			}
			// since file sinks are lazy, we trigger the execution explicitly
			env.execute("KDDTree Outlier Detection");
		} catch (Exception e) {

		}
	}

	/**
	 * Find the outliers using IQR method. All the distances are passed and the
	 * points with outlier distance are considered as outliers, i.e. if the
	 * point is relatively very far from it's nearest point then it should most
	 * likely be an outlier.
	 */
	public static final class findOutliers extends RichMapFunction<Tuple2<Point, Double>, Tuple2<Point, Boolean>> {

		private Collection<Double> distance;

		@Override
		public void open(Configuration parameters) throws Exception {
			this.distance = getRuntimeContext().getBroadcastVariable("distance");

		}

		@Override
		public Tuple2<Point, Boolean> map(Tuple2<Point, Double> point) throws Exception {
			List<Double> distanceList = new ArrayList<Double>();
			distanceList.addAll(distance);
			Collections.sort(distanceList, new DistanceComparator());
			double boundary[] = new double[2];
			boundary = BaseFunctions.getLowerAndUpperBound(distanceList);
			boolean outlier = false;
			if (point.f1 < boundary[0] || point.f1 > boundary[1]) {
				// set as outlier
				outlier = true;
			}
			return new Tuple2<Point, Boolean>(point.f0, outlier);
		}
	}

	/**
	 * Converts points with distance to distance only
	 */
	public static final class TuplePointToDistance implements MapFunction<Tuple2<Point, Double>, Double> {

		@Override
		public Double map(Tuple2<Point, Double> t) throws Exception {
			return new Double(t.f1);
		}
	}

	/**
	 * Find distance of the point to its nearest Point
	 * 
	 */
	public static final class findDistanceToNearestPoint extends RichMapFunction<Point, Tuple2<Point, Double>> {

		/**
		 * 
		 */
		// private static final long serialVersionUID = 1L;
		private Collection<Point> points;
		// private List<Point> newPoints; // this collection of point is
		// without the own point
		// otherwise the KDDTree will say the nearest point is same.
		int dim = 0;

		@Override
		public void open(Configuration parameters) throws Exception {
			this.points = getRuntimeContext().getBroadcastVariable("points");

		}

		// dim means dimension
		public findDistanceToNearestPoint(int dim) {
			this.dim = dim;
		}

		@Override
		public Tuple2<Point, Double> map(Point point) throws Exception {
			List<Point> newPoints = new ArrayList<Point>();
			for (Point p : points) {
				if (!p.equals(point)) {
					newPoints.add(p);
				}
			}
			// now call KDDTree to find the closest point
			KDTree kdt = new KDTree(newPoints.size(), dim);
			double x[] = new double[dim];
			for (Point p : newPoints) {
				x = ArrayUtils.toPrimitive(p.pt);
				kdt.add(x, dim);
			}
			double s[] = new double[dim];
			s = ArrayUtils.toPrimitive(point.pt);
			KDNode kdn = kdt.find_nearest(s, dim);
			x = kdn.x;
			return new Tuple2<Point, Double>(point, BaseFunctions.euclideanDistance(s, x));
		}
	}

	private static DataSet<String> getTextDataSet(ExecutionEnvironment env) {
		if (fileOutput) {
			// read the text file from given input path
			return env.readTextFile(pointsPath);
		} else {
			// get default test text data
			return null;
		}
	}

	// ***********************************************
	// Input: <input file path> <output file path> <number of columns in file i.e. dimension>
	// ***********************************************
	private static boolean fileOutput = false;
	private static String pointsPath = null;
	private static String outputPath = null;
	private static int dim = 0;

	private static boolean parseParameters(String[] programArguments) {

		if (programArguments.length > 0) {
			// parse input arguments
			fileOutput = true;
			if (programArguments.length == 3) {
				pointsPath = programArguments[0];
				outputPath = programArguments[1];
				dim = Integer.parseInt(programArguments[2]);
			} else {
				System.err.println("Usage: KMedians <input file path> <output file path> <number of columns in file i.e. dimension>");
				return false;
			}
		} else {
			System.out.println("Executing KD-Tree example with default parameters and built-in default data.");
			System.out.println("  Provide parameters to read input data from files.");
			System.out.println("  See the documentation for the correct format of input files.");
			System.out.println("  We provide a data generator to create synthetic input files for this program.");
			System.out.println("  Usage: KMeans <points path> <centers path> <result path> <num iterations>");
		}
		return true;
	}

}
