package com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch;

//This is a java program to find nearest neighbor using KD Tree implementation
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
 
class KDNode
{
    int axis;
    double[] x;
    int id;
    boolean checked;
    boolean orientation;
 
    KDNode Parent;
    KDNode Left;
    KDNode Right;
 
    public KDNode(double[] x0, int axis0, int dim)
    {
        x = new double[dim];
        axis = axis0;
        for (int k = 0; k < dim; k++)
            x[k] = x0[k];
 
        Left = Right = Parent = null;
        checked = false;
        id = 0;
    }
 
    public KDNode FindParent(double[] x0)
    {
        KDNode parent = null;
        KDNode next = this;
        int split;
        while (next != null)
        {
            split = next.axis;
            parent = next;
            if (x0[split] > next.x[split])
                next = next.Right;
            else
                next = next.Left;
        }
        return parent;
    }
 
    public KDNode Insert(double[] p, int dim)
    {
        //x = new double[2];
        KDNode parent = FindParent(p);
        if (equal(p, parent.x, dim) == true)
            return null;
 
        KDNode newNode = new KDNode(p, parent.axis + 1 < dim ? parent.axis + 1
                : 0, dim);
        newNode.Parent = parent;
 
        if (p[parent.axis] > parent.x[parent.axis])
        {
            parent.Right = newNode;
            newNode.orientation = true; //
        } else
        {
            parent.Left = newNode;
            newNode.orientation = false; //
        }
 
        return newNode;
    }
 
    boolean equal(double[] x1, double[] x2, int dim)
    {
        for (int k = 0; k < dim; k++)
        {
            if (x1[k] != x2[k])
                return false;
        }
 
        return true;
    }
 
    double distance2(double[] x1, double[] x2, int dim)
    {
        double S = 0;
        for (int k = 0; k < dim; k++)
            S += (x1[k] - x2[k]) * (x1[k] - x2[k]);
        return S;
    }
}
 
class KDTree
{
    KDNode Root;
 
    int TimeStart, TimeFinish;
    int CounterFreq;
 
    double d_min;
    KDNode nearest_neighbour;
 
    int KD_id;
 
    int nList;
 
    KDNode CheckedNodes[];
    int checked_nodes;
    KDNode List[];
 
    double x_min[], x_max[];
    boolean max_boundary[], min_boundary[];
    int n_boundary;
 
    public KDTree(int i, int dim)
    {
        Root = null;
        KD_id = 1;
        nList = 0;
        List = new KDNode[i];
        CheckedNodes = new KDNode[i];
        max_boundary = new boolean[dim];
        min_boundary = new boolean[dim];
        x_min = new double[dim];
        x_max = new double[dim];
    }
 
    public boolean add(double[] x, int dim)
    {
        if (nList >= 2000000 - 1)
            return false; // can't add more points
 
        if (Root == null)
        {
            Root = new KDNode(x, 0, dim);
            Root.id = KD_id++;
            List[nList++] = Root;
        } else
        {
            KDNode pNode;
            if ((pNode = Root.Insert(x, dim)) != null)
            {
                pNode.id = KD_id++;
                List[nList++] = pNode;
            }
        }
 
        return true;
    }
 
    public KDNode find_nearest(double[] x, int dim)
    {
        if (Root == null)
            return null;
 
        checked_nodes = 0;
        KDNode parent = Root.FindParent(x);
        nearest_neighbour = parent;
        d_min = Root.distance2(x, parent.x, dim);
 
        if (parent.equal(x, parent.x, dim) == true)
            return nearest_neighbour;
 
        search_parent(parent, x, dim);
        uncheck();
 
        return nearest_neighbour;
    }
 
    public void check_subtree(KDNode node, double[] x, int dim)
    {
        if ((node == null) || node.checked)
            return;
 
        CheckedNodes[checked_nodes++] = node;
        node.checked = true;
        set_bounding_cube(node, x, dim);
 
        dim = node.axis;
        double d = node.x[dim] - x[dim];
 
        if (d * d > d_min)
        {
            if (node.x[dim] > x[dim])
                check_subtree(node.Left, x, dim);
            else
                check_subtree(node.Right, x, dim);
        } else
        {
            check_subtree(node.Left, x, dim);
            check_subtree(node.Right, x, dim);
        }
    }
 
    public void set_bounding_cube(KDNode node, double[] x, int dim)
    {
        if (node == null)
            return;
        int d = 0;
        double dx;
        for (int k = 0; k < dim; k++)
        {
            dx = node.x[k] - x[k];
            if (dx > 0)
            {
                dx *= dx;
                if (!max_boundary[k])
                {
                    if (dx > x_max[k])
                        x_max[k] = dx;
                    if (x_max[k] > d_min)
                    {
                        max_boundary[k] = true;
                        n_boundary++;
                    }
                }
            } else
            {
                dx *= dx;
                if (!min_boundary[k])
                {
                    if (dx > x_min[k])
                        x_min[k] = dx;
                    if (x_min[k] > d_min)
                    {
                        min_boundary[k] = true;
                        n_boundary++;
                    }
                }
            }
            d += dx;
            if (d > d_min)
                return;
 
        }
 
        if (d < d_min)
        {
            d_min = d;
            nearest_neighbour = node;
        }
    }
 
    public KDNode search_parent(KDNode parent, double[] x, int dim)
    {
        for (int k = 0; k < dim; k++)
        {
            x_min[k] = x_max[k] = 0;
            max_boundary[k] = min_boundary[k] = false; //
        }
        n_boundary = 0;
 
        KDNode search_root = parent;
        while (parent != null && (n_boundary != 2 * 2))
        {
            check_subtree(parent, x, dim);
            search_root = parent;
            parent = parent.Parent;
        }
 
        return search_root;
    }
 
    public void uncheck()
    {
        for (int n = 0; n < checked_nodes; n++)
            CheckedNodes[n].checked = false;
    }
 
}
 
public class KDTNearest
{
 
	
    public static void main(String args[]) throws IOException
    {
        BufferedReader in = new BufferedReader(new FileReader("/home/subash/Desktop/input.txt"));
        int numpoints = 5;
        System.out
        .println("Enter the co-ordinates of the point: (one after the other)");
InputStreamReader reader = new InputStreamReader(System.in);
BufferedReader br = new BufferedReader(reader);
int dim = Integer.parseInt(br.readLine());
        KDTree kdt = new KDTree(numpoints, dim);
        double x[] = new double[dim];
 
        x[0] = 2.1;
        x[1] = 4.3;
        x[2] = 3.3;
        kdt.add(x, dim);
 
        x[0] = 3.3;
        x[1] = 1.5;
        x[2] = 12.3;
        kdt.add(x, dim);
 
        x[0] = 4.7;
        x[1] = 11.1;
        x[2] = 5.3;
        kdt.add(x, dim);
 
        x[0] = 5.0;
        x[1] = 12.3;
        x[2] = 4.3;
        kdt.add(x, dim);
 
        x[0] = 5.1;
        x[1] = 1.2;
        x[2] = 1.3;
        kdt.add(x, dim);
 
        double s[] = new double[dim];
        for(int i=0;i<dim;i++){
        	s[i] = Double.parseDouble(br.readLine());
        }
        
        KDNode kdn = kdt.find_nearest(s, dim);
        System.out.println("The nearest neighbor is: ");
        System.out.println("(" + kdn.x[0] + " , " + kdn.x[1]+ " , " + kdn.x[2]+")");
        in.close();
    }
}