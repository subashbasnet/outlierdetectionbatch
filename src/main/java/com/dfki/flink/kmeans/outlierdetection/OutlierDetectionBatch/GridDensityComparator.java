package com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch;

import java.util.Comparator;

import org.apache.flink.api.java.tuple.Tuple3;

import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.GBODAgataBatch.Grid;

public class GridDensityComparator implements Comparator<Tuple3<Grid, Double, Long>> {

	/**
	 * 
	 */

	GridDensityComparator() {
	}

	/**
	 * @return -1, 1
	 * @param o1
	 * @param o2
	 *            computes the tuple result in Ascending order comparing the f1
	 *            values
	 */
	@Override
	public int compare(Tuple3<Grid, Double, Long> o1, Tuple3<Grid, Double, Long> o2) {
		if (o1.f1 < o2.f1) {
			return -1;
		} else {
			return 1;
		}
	}
}
