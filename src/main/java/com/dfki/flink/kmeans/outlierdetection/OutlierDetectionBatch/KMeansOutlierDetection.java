package com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.functions.FunctionAnnotation.ForwardedFields;
import org.apache.flink.api.java.operators.IterativeDataSet;
import org.apache.flink.api.java.operators.UnsortedGrouping;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;

/**
 *KMeans Clustering, Outlier Detection.
 */
@SuppressWarnings("serial")
public class KMeansOutlierDetection {

	// todo: take no of centroid inputs and limit the number of centroids to
	// that number
	private static int rowsCount = 0;

	public static void main(String[] args) throws Exception {

		if (!parseParameters(args)) {
			return;
		}

		// set up execution environment
		ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		// get input data
		DataSet<String> data = getTextDataSet(env);
		DataSet<Tuple2<Integer, Double[]>> newDataSet =
		data.flatMap(new Tokenizer());
		DataSet<Point> points = newDataSet.map(new getPoints());
		
		// todo: take this no. e.g.: 10, as input from user for no of centroids
		DataSet<Centroid> centroids = newDataSet.map(new TupleCentroidConverter()).first(10);
		//centroids.print();
		// DataSet<Centroid> centroids = getCentroidDataSet(env);
		
		// set number of bulk iterations for KMeans algorithm
		IterativeDataSet<Centroid> loop = centroids.iterate(numIterations);
		
		//group by points is done on the basis of centroids they belong to,
		//then the new centroids is calculated by mean method
		//i.e. sum of those points divided by the count of those points belonging
		//to that particular centroid, now that old centroid gets changed to
		//new centroid with more accurate value
		DataSet<Centroid> newCentroids = points
				// compute closest centroid for each point
				.map(new SelectNearestCenter()).withBroadcastSet(loop, "centroids")
				// count and sum point coordinates for each centroid
				.map(new CountAppender()).groupBy(0).reduce(new CentroidAccumulator())
				// compute new centroids from point counts and coordinate sums
				.map(new CentroidAverager());
		// feed new centroids back into next iteration
		DataSet<Centroid> finalCentroids = loop.closeWith(newCentroids);
		//recalculating the points where they belong to with respect to the newly found out centroids by applying mean method.
		DataSet<Tuple2<Integer, Point>> clusteredPoints = points
				// assign points to final clusters
				.map(new SelectNearestCenter()).withBroadcastSet(finalCentroids, "centroids");
		DataSet<ClusteredPoint> newClusteredPoints = clusteredPoints.map(new TupleClusteredPointConverter());
		DataSet<Tuple2<Point, Boolean>> distancePoints = newClusteredPoints
				.map(new calculateDistanceToCenter()).withBroadcastSet(finalCentroids, "centroids")
				.withBroadcastSet(clusteredPoints, "clusteredPoints");
		if (fileOutput) {
			distancePoints.writeAsCsv(outputPath, "\n", " ");
			// since file sinks are lazy, we trigger the execution explicitly
			env.execute("KMeans Outlier Detection");
		} else {
			distancePoints.print();
		}
	}

	/** Determines the distance of center for a data point. */
	public static final class calculateDistanceToCenter
			extends RichMapFunction<ClusteredPoint, Tuple2<Point, Boolean>> {
		private Collection<Centroid> centroids;
		private Collection<Tuple2<Integer, Point>> clusteredPoints;

		/**
		 * Reads the centroid values from a broadcast variable into a
		 * collection.
		 */
		@Override
		public void open(Configuration parameters) throws Exception {
			this.centroids = getRuntimeContext().getBroadcastVariable("centroids");
			this.clusteredPoints = getRuntimeContext().getBroadcastVariable("clusteredPoints");
		}

		/**
		 * Takes clusteredPoint p, i.e. a single point as input. Perform
		 * operations to detect whether this point is an out-lier or not.
		 * Returns with a boolean value if this point is an out-lier or not.
		 * This method gets called parallel for each points until the completion
		 * of execution for all points. The result is parallel written to the
		 * file by each node cluster.
		 **/
		@Override
		public Tuple2<Point, Boolean> map(ClusteredPoint p) throws Exception {
			List<Tuple3<Centroid, Tuple2<Integer, Point>, Double>> elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<Integer, Point>, Double>>();
			Tuple3<Integer, Point, Boolean> returnElement = new Tuple3<Integer, Point, Boolean>();// true
			elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<Integer, Point>, Double>>();
			Tuple3<Centroid, Tuple2<Integer, Point>, Double> newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
			Centroid centroid = new Centroid();
			for (Tuple2<Integer, Point> e : clusteredPoints) {
				// compute distance
				if (e.f0 == p.id) {// comparing centroid ids
					newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
					for (Centroid cent : centroids) {
						if (e.f0 == cent.id) {
							centroid = cent;
						}
					}
					double distance = e.f1.euclideanDistance(centroid);
					newElement.setFields(centroid, e, distance);
					elementsWithDistance.add(newElement);
				}
			}
			// sorting the distances in ascending order
			Collections.sort(elementsWithDistance, new Tuple3Comparator());
			// finding q1 and q3 for even number of element counts;
			double q1 = 0;
			double q3 = 0;
			// only calculate if size is greater than equal to 2
			if (elementsWithDistance.size() >= 2) {
				q1 = BaseFunctions.calcluateQ1(elementsWithDistance);
				q3 = BaseFunctions.calcluateQ3(elementsWithDistance);
			}

			// find IQR
			double iqr = q3 - q1;
			// multiplying iqr by 1.5 to find bounds is standard practice
			double lowerbound = q1 - 1.5 * iqr;
			double upperbound = q3 + 1.5 * iqr;
			// out-lier
			boolean elementSet = false;
			for (Tuple3<Centroid, Tuple2<Integer, Point>, Double> elementWithDistance : elementsWithDistance) {
				returnElement = new Tuple3<Integer, Point, Boolean>();
				if (Arrays.equals(p.pt, elementWithDistance.f1.f1.pt)) {// comparing
																		// values
					// of points, to
					// verify if it's
					// the same element
					if (elementWithDistance.f2 < lowerbound || elementWithDistance.f2 > upperbound) {
						// set as outlier
						returnElement.setFields(elementWithDistance.f1.f0, elementWithDistance.f1.f1, true);
					} else {
						returnElement.setFields(elementWithDistance.f1.f0, elementWithDistance.f1.f1, false);
					}
					elementSet = true;
				}
				if (elementSet) {
					break;
				}
			}
			// emit a new record with the center id, the data point and distance
			// to the center.
			return new Tuple2<Point, Boolean>(p, returnElement.f2);
		}
	}

	
//	/** Determines the distance of center for a data point. */
//	public static final class calculateDistanceToCenter
//			extends RichMapFunction<ClusteredPoint, Tuple2<Point, Boolean>> {
//		private Collection<Centroid> centroids;
//		private Collection<Tuple2<Integer, Point>> clusteredPoints;
//
//		/**
//		 * Reads the centroid values from a broadcast variable into a
//		 * collection.
//		 */
//		@Override
//		public void open(Configuration parameters) throws Exception {
//			this.centroids = getRuntimeContext().getBroadcastVariable("centroids");
//			this.clusteredPoints = getRuntimeContext().getBroadcastVariable("clusteredPoints");
//		}
//
//		/**
//		 * Takes clusteredPoint p, i.e. a single point as input. Perform
//		 * operations to detect whether this point is an out-lier or not.
//		 * Returns with a boolean value if this point is an out-lier or not.
//		 * This method gets called parallel for each points until the completion
//		 * of execution for all points. The result is parallel written to the
//		 * file by each node cluster.
//		 **/
//		@Override
//		public Tuple2<Point, Boolean> map(ClusteredPoint p) throws Exception {
//			List<Tuple3<Centroid, Tuple2<Integer, Point>, Double>> elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<Integer, Point>, Double>>();
//			Tuple3<Integer, Point, Boolean> returnElement = new Tuple3<Integer, Point, Boolean>();// true
//			elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<Integer, Point>, Double>>();
//			Tuple3<Centroid, Tuple2<Integer, Point>, Double> newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
//			Centroid centroid = new Centroid();
//			double totalDistance = 0;
//			 int elementsCount = 0;
//			for (Tuple2<Integer, Point> e : clusteredPoints) {
//				// compute distance
//				if (e.f0 == p.id) {// comparing centroid ids
//					newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
//					for (Centroid cent : centroids) {
//						if (e.f0 == cent.id) {
//							centroid = cent;
//						}
//					}
//					double distance = e.f1.euclideanDistance(centroid);
//					 totalDistance += distance;
//					newElement.setFields(centroid, e, distance);
//					elementsWithDistance.add(newElement);
//					elementsCount++;
//				}
//			}
//			// finding mean
//			 double mean = totalDistance / elementsCount;
//			// sorting the distances in ascending order
//			Collections.sort(elementsWithDistance, new Tuple3Comparator());
//			double sdTotalDistanceSquare = 0;
//			 for (Tuple3<Centroid, Tuple2<Integer, Point>, Double> elementWithDistance
//			 : elementsWithDistance) {
//			 double distanceSquare = Math.pow(mean - elementWithDistance.f2, 2);
//			 sdTotalDistanceSquare += distanceSquare;
//			 }
//			 double sd = Math.sqrt(sdTotalDistanceSquare / elementsCount);
//			 double upperbound = mean + 2 * sd;
//			 double lowerbound = mean - 2 * sd;
//			// out-lier
//			boolean elementSet = false;
//			for (Tuple3<Centroid, Tuple2<Integer, Point>, Double> elementWithDistance : elementsWithDistance) {
//				returnElement = new Tuple3<Integer, Point, Boolean>();
//				if (Arrays.equals(p.pt, elementWithDistance.f1.f1.pt)) {// comparing
//																		// values
//					// of points, to
//					// verify if it's
//					// the same element
//					if (elementWithDistance.f2 < lowerbound || elementWithDistance.f2 > upperbound) {
//						// set as outlier
//						returnElement.setFields(elementWithDistance.f1.f0, elementWithDistance.f1.f1, true);
//					} else {
//						returnElement.setFields(elementWithDistance.f1.f0, elementWithDistance.f1.f1, false);
//					}
//					elementSet = true;
//				}
//				if (elementSet) {
//					break;
//				}
//			}
//			// emit a new record with the center id, the data point and distance
//			// to the center.
//			return new Tuple2<Point, Boolean>(p, returnElement.f2);
//		}
//	}
	
	
	
	/**
	 * General Method of doing outlier detection which does not work properly
	 * i.e. doesn't print the files, as it's not using the dataset i.e. the
	 * cluster, but it's doing everything in client using .collect() method to
	 * retrieve the dataset as list and perform operations on that
	 **/
//	 @SuppressWarnings("rawtypes")
//	 public static List<Tuple3> findOutliers(DataSet<Tuple2<Integer, Point>>
//	 clusteredPoints,
//	 DataSet<Centroid> centroids, ExecutionEnvironment env) throws Exception {
//	 DataSet<Tuple3<Integer, Point, Boolean>> dataSetElement = null;
//	 int count = 0;
//	 List<Tuple3> finalElements = new ArrayList<Tuple3>();
//	 List<Tuple2<Integer, Point>> elements = clusteredPoints.collect();
//	 List<Centroid> centroidList = centroids.collect();
//	 List<Tuple3<Centroid, Tuple2<Integer, Point>, Double>>
//	 elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<Integer,
//	 Point>, Double>>();
//	 for (Centroid centroid : centroidList) {
//	 elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<Integer,
//	 Point>, Double>>();
//	 double totalDistance = 0;
//	 int elementsCount = 0;
//	 for (Tuple2<Integer, Point> e : elements) {
//	 // compute distance
//	 if (e.f0 == centroid.id) {
//	
//	 Tuple3<Centroid, Tuple2<Integer, Point>, Double> newElement = new
//	 Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
//	 double distance = e.f1.euclideanDistance(centroid);
//	 totalDistance += distance;
//	 newElement.setFields(centroid, e, distance);
//	 elementsWithDistance.add(newElement);
//	 elementsCount++;
//	 }
//	 }
//	 // finding mean
//	 double mean = totalDistance / elementsCount;
//	 double sdTotalDistanceSquare = 0;
//	 for (Tuple3<Centroid, Tuple2<Integer, Point>, Double> elementWithDistance
//	 : elementsWithDistance) {
//	 double distanceSquare = Math.pow(mean - elementWithDistance.f2, 2);
//	 sdTotalDistanceSquare += distanceSquare;
//	 }
//	 double sd = Math.sqrt(sdTotalDistanceSquare / elementsCount);
//	 double upperlimit = mean + 2 * sd;
//	 double lowerlimit = mean - 2 * sd;
//	 Tuple3<Integer, Point, Boolean> newElement = new Tuple3<Integer, Point,
//	 Boolean>();// true
//	 // =
//	 // outlier
//	 System.out.println("upperlimit-->" + upperlimit);
//	 System.out.println("lowerlimit-->" + lowerlimit);
//	 for (Tuple3<Centroid, Tuple2<Integer, Point>, Double> elementWithDistance
//	 : elementsWithDistance) {
//	 newElement = new Tuple3<Integer, Point, Boolean>();
//	 if (elementWithDistance.f2 < lowerlimit || elementWithDistance.f2 >
//	 upperlimit) {
//	 // set as outlier
//	 newElement.setFields(elementWithDistance.f1.f0,
//	 elementWithDistance.f1.f1, true);
//	 } else {
//	 newElement.setFields(elementWithDistance.f1.f0,
//	 elementWithDistance.f1.f1, false);
//	 }
//	 finalElements.add(newElement);
//	 if (count == 0) {
//	 dataSetElement = env.fromElements(newElement);
//	 dataSetElement.print();
//	 } else {
//	 dataSetElement.join(env.fromElements(newElement));
//	 dataSetElement.print();
//	 }
//	 count++;
//	 }
//	 }
//	 return finalElements;
//	 }

	// *************************************************************************
	// DATA TYPES
	// *************************************************************************

	public static class ClusteredPoint extends Point {

		public int id;

		public ClusteredPoint() {
		}

		public ClusteredPoint(int id, Point p) {
			super(p.pt);
			this.id = id;
		}

		public ClusteredPoint(int id, Double pt[]) {
			super(pt);
			this.id = id;
		}

		@Override
		public String toString() {
			return id + " " + super.toString();
		}
	}

	/**
	 * A multi-dimensional point.
	 */
	public static class Point implements Serializable {

		public Double pt[];

		public Point() {
		}

		public Point(Double pt[]) {
			this.pt = pt;
		}

		public Point add(Point other) {
			for (int i = 0; i < other.pt.length; i++) {
				pt[i] += other.pt[i];
			}
			return this;
		}
		
		/**
		 * Eg: 3-d value means:
		 * Suppose a centroid has 2 elments, (2,3,2) and (3,2,3)
		 * then return new centroid as ((2+3)/2,(3+2)/2,(2+3)/2
		 * */
		public Point div(long val) {
			for (int i = 0; i < pt.length; i++) {
				pt[i] /= val;
			}
			return this;
		}

		public double euclideanDistance(Point other) {
			double distance = 0;
			for (int i = 0; i < other.pt.length; i++) {
				distance += (pt[i] - other.pt[i]) * (pt[i] - other.pt[i]);
			}
			return Math.sqrt(distance);
		}

		public void clear() {
			Arrays.fill(pt, 0);
		}

		@Override
		public String toString() {
			String distance = "";
			for (int i = 0; i < pt.length; i++) {
				distance = distance + " " + pt[i];
			}
			return distance;
		}
	}

	/**
	 * A simple two-dimensional centroid, basically a point with an ID.
	 */
	public static class Centroid extends Point {

		public int id;

		public Centroid() {
		}

		public Centroid(int id, Double pt[]) {
			super(pt);
			this.id = id;
		}

		public Centroid(int id, Point p) {
			super(p.pt);
			this.id = id;
		}

		@Override
		public String toString() {
			return id + " " + super.toString();
		}
	}

	// *************************************************************************
	// USER FUNCTIONS
	// *************************************************************************

	/** Converts a {@code Tuple2<Double,Double>} into a Point. */
	public static final class TuplePointConverter implements MapFunction<Double[], Point> {

		@Override
		public Point map(Double t[]) throws Exception {
			
			return new Point(t);
		}
	}

	/** Converts a {@code Tuple3<Integer, Double,Double>} into a Centroid. */
	@ForwardedFields("0->id;")
	public static final class TupleCentroidConverter implements MapFunction<Tuple2<Integer, Double[]>, Centroid> {

		@Override
		public Centroid map(Tuple2<Integer, Double[]> ct) throws Exception {
			return new Centroid(ct.f0, ct.f1);
		}
	}

	/** Converts a {@code Tuple3<Integer, Double,Double>} into a Centroid. */
	// @ForwardedFields("0->id; 1->x; 2->y")
	public static final class TupleClusteredPointConverter
			implements MapFunction<Tuple2<Integer, Point>, ClusteredPoint> {

		@Override
		public ClusteredPoint map(Tuple2<Integer, Point> t) throws Exception {
			return new ClusteredPoint(t.f0, t.f1);
		}
	}
	
	/** Determines the closest cluster center for a data point. */
	@ForwardedFields("*->1")
	public static final class SelectNearestCenter extends RichMapFunction<Point, Tuple2<Integer, Point>> {
		private Collection<Centroid> centroids;
		/**
		 * Reads the centroid values from a broadcast variable into a
		 * collection.
		 */
		@Override
		public void open(Configuration parameters) throws Exception {
			this.centroids = getRuntimeContext().getBroadcastVariable("centroids");
		}
		@Override
		public Tuple2<Integer, Point> map(Point p) throws Exception {
			double minDistance = Double.MAX_VALUE;
			int closestCentroidId = -1;
			// check all cluster centers
			for (Centroid centroid : centroids) {
				// compute distance
				double distance = p.euclideanDistance(centroid);
				// update nearest cluster if necessary
				if (distance < minDistance) {
					minDistance = distance;
					closestCentroidId = centroid.id;
				}
			}
			// emit a new record with the center id and the data point.
			return new Tuple2<Integer, Point>(closestCentroidId, p);
		}
	}

	/** converts Tuple2<Integer, double[]> to Point */
	// @ForwardedFields("0->id")
	public static final class getPoints implements MapFunction<Tuple2<Integer, Double[]>, Point> {

		@Override
		public Point map(Tuple2<Integer, Double[]> value) {
			return new Point(value.f1);
		}
	}

	/**
	 * Splits sentences into words as a
	 * user-defined FlatMapFunction. The function takes a line (String) and
	 * splits it into multiple pairs in the form of "(1, Double[])" (
	 * {@code Tuple2<String, Double[]>}).
	 */
	public static final class Tokenizer implements FlatMapFunction<String, Tuple2<Integer, Double[]>> {

		@Override
		public void flatMap(String value, Collector<Tuple2<Integer, Double[]>> out) {
			// normalize and split the line
			//System.out.println("value-->"+value);
			String[] tokens = value.toLowerCase().split("\\s*,\\s*",-1);
			
			Double[] columns = new Double[tokens.length];

			for (int i = 0; i < tokens.length; i++) {
				if (!(tokens[i] == null) && !(tokens[i].length() == 0)) {
					columns[i] = Double.parseDouble(tokens[i]);
				} else {
					columns[i] = 0.0;
				}
				// if (!tokens[i].equals("")) {
				// columns[i] = Double.parseDouble(tokens[i]);
				// }
			}
			out.collect(new Tuple2<Integer, Double[]>(rowsCount + 1, columns));
			rowsCount++;
		}
	}

	/** Appends a count variable to the tuple. */
	@ForwardedFields("f0;f1")
	public static final class CountAppender
			implements MapFunction<Tuple2<Integer, Point>, Tuple3<Integer, Point, Long>> {

		@Override
		public Tuple3<Integer, Point, Long> map(Tuple2<Integer, Point> t) {
			return new Tuple3<Integer, Point, Long>(t.f0, t.f1, 1L);
		}
	}

	/** Sums and counts point coordinates. 
	 * group by above was done for every points belonging 
	 * to a particular centroid,
	 * now calculate the sum and count of those points belonging to
	 * that centroid, this way later we can get average of those points
	 * belonging to particular centroid to find the new centroids
	 * returns a single value as a result of reduce function finally
	 * */
	@ForwardedFields("0")
	public static final class CentroidAccumulator implements ReduceFunction<Tuple3<Integer, Point, Long>> {

		@Override
		public Tuple3<Integer, Point, Long> reduce(Tuple3<Integer, Point, Long> val1,
				Tuple3<Integer, Point, Long> val2) {
			return new Tuple3<Integer, Point, Long>(val1.f0, val1.f1.add(val2.f1), val1.f2 + val2.f2);
		}
	}

	/** Computes new centroid from coordinate sum and count of points.
	 * here, new centroids for those points belonging to particular centroid is 
	 * calculated by diving the sum of points by the number of points
	 * i.e. by finding the average of those points,
	 * the single value from reduce function, numerator/denominator to find
	 * new centroid point
	 *  */
	@ForwardedFields("0->id")
	public static final class CentroidAverager implements MapFunction<Tuple3<Integer, Point, Long>, Centroid> {

		@Override
		public Centroid map(Tuple3<Integer, Point, Long> value) {
			return new Centroid(value.f0, value.f1.div(value.f2));
		}
	}

	// *************************************************************************
	// UTIL METHODS
	// *************************************************************************

	private static boolean fileOutput = false;
	private static String pointsPath = null;
	// private static String centersPath = null;
	private static String outputPath = null;
	private static int numIterations = 10;
	// private static int numCentroids = 10;

	private static boolean parseParameters(String[] programArguments) {

		if (programArguments.length > 0) {
			// parse input arguments
			fileOutput = true;
			if (programArguments.length == 4) {
				pointsPath = programArguments[0];
				// centersPath = programArguments[1];
				outputPath = programArguments[2];
				numIterations = Integer.parseInt(programArguments[3]);
				// numCentroids = Integer.parseInt(programArguments[3]);
			} else {
				System.err.println("Usage: KMeans <points path> <centers path> <result path> <num iterations>");
				return false;
			}
		} else {
			System.out.println("  Usage: KMeans <points path> <centers path> <result path> <num iterations>");
		}
		return true;
	}

	// private static DataSet<Point> getPointDataSet(ExecutionEnvironment env) {
	// if (fileOutput) {
	// // read points from CSV file
	// return
	// env.readCsvFile(pointsPath).fieldDelimiter(",").includeFields(false,
	// true, true)
	// .types(Double.class, Double.class).map(new TuplePointConverter());
	// } else {
	// return KMeansOutlierDetectionData.getDefaultPointDataSet(env);
	// }
	// }

	// Note: the issue is I need to remove the first row of the csv file i.e.
	// the header with row names
	// as it'll throw exception while converting to numbers later
	// I also need to know each column is either string or number before I read
	// the csv file.
	private static DataSet<String> getTextDataSet(ExecutionEnvironment env) {
		if (fileOutput) {
			// read the text file from given input path
			return env.readTextFile(pointsPath);
		} else {
			// get default test text data
			return null;
		}
	}

	// private static DataSet<Centroid> getCentroidDataSet(ExecutionEnvironment
	// env) {
	// if (fileOutput) {
	// return
	// env.readCsvFile(centersPath).fieldDelimiter("").includeFields(true, true,
	// true)
	// .types(Integer.class, Double.class, Double.class).map(new
	// TupleCentroidConverter());
	// } else {
	// return KMeansOutlierDetectionData.getDefaultCentroidDataSet(env);
	// }
	// }
}
