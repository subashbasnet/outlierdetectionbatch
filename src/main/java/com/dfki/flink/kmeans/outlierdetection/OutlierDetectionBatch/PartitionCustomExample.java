package com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch;

import org.apache.flink.api.common.functions.Partitioner;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;

public class PartitionCustomExample {
	public static class MyPartitioner implements Partitioner<Integer> {
		@Override
		public int partition(Integer key, int numPartitions) {
			return key % numPartitions;
		}
	}

	public static void main(String[] args) throws Exception {
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		DataSet<Tuple2<Integer, String>> data = env.fromElements(new Tuple2<Integer, String>(1, "a"),
				new Tuple2<Integer, String>(1000, "b"), new Tuple2<Integer, String>(2, "k"),
				new Tuple2<Integer, String>(1020, "c"));
		data.print();
		DataSet<Tuple2<Integer, String>> partitionedData = data.sortPartition(0, Order.DESCENDING);

		partitionedData.print();
	}
}
