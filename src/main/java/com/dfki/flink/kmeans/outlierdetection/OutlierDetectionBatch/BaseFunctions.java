package com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch;

import java.util.ArrayList;
import java.util.List;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;

import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.GBODAgataBatch.Grid;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.Centroid;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.Point;

public class BaseFunctions {

	public static void main(String args[]) {
		int intArray[] = { 4, -1, -8, -9 };
		List<Tuple3<Centroid, Tuple2<Integer, Point>, Double>> elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<Integer, Point>, Double>>();
		Tuple3<Centroid, Tuple2<Integer, Point>, Double> newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
		newElement.setFields(null, null, 2.0);
		elementsWithDistance.add(newElement);
		newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
		newElement.setFields(null, null, 4.0);
		elementsWithDistance.add(newElement);
		// newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
		// newElement.setFields(null, null, 5.0);
		// elementsWithDistance.add(newElement);
		// newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
		// newElement.setFields(null, null, 6.0);
		// elementsWithDistance.add(newElement);
		// newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
		// newElement.setFields(null, null, 8.0);
		// elementsWithDistance.add(newElement);
		double median = calcluateMedian(elementsWithDistance);
		double q1 = calcluateQ1(elementsWithDistance);
		double q3 = calcluateQ3(elementsWithDistance);
		System.out.println(median);
		System.out.println(q1);
		System.out.println(q3);
	}

	/**
	 * Eg: 3-d value means: Suppose a centroid has 2 elments, (2,3,2) and
	 * (3,2,3) then return new centroid as ((2+3)/2,(3+2)/2,(2+3)/2).
	 */
	public static Double[] div(long val, Double[] pt1, Double[] pt2) {
		Double pt[] = new Double[pt1.length];
		for (int i = 0; i < pt1.length; i++) {
			pt[i] = (pt1[i] + pt2[i]) / val;
		}
		return pt;
	}

	public static Tuple3<Centroid, Tuple2<Integer, Point>, Double> calculateMedian(
			List<Tuple3<Centroid, Tuple2<Integer, Point>, Double>> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for even number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {
			double median = (elementsWithDistance.get(size / 2 - 1).f2 + elementsWithDistance.get(size / 2 + 1 - 1).f2)
					/ 2;
			// double lower_difference = median - elementsWithDistance.get(size
			// / 2 - 1).f2;
			// double upper_difference = elementsWithDistance.get(size / 2 + 1 -
			// 1).f2 - median;
			//
			// if (upper_difference >= lower_difference) {
			// return elementsWithDistance.get(size / 2 + 1 - 1);
			// }
			Point p = new Point(div(2, elementsWithDistance.get(size / 2 - 1).f0.pt,
					elementsWithDistance.get(size / 2 + 1 - 1).f0.pt));
			Centroid c = new Centroid(elementsWithDistance.get(size / 2 - 1).f0.id, p);
			Tuple2<Integer, Point> pt = new Tuple2<Integer, Point>(elementsWithDistance.get(size / 2 - 1).f1.f0, p);
			return new Tuple3<Centroid, Tuple2<Integer, Point>, Double>(c, pt, median);
		} else// for odd number of elements, '1' is deducted as index starts
				// from zero
			return elementsWithDistance.get(((size + 1) / 2) - 1);
	}

	public static double calcluateMedian(List<Tuple3<Centroid, Tuple2<Integer, Point>, Double>> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for even number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {
			return (elementsWithDistance.get(size / 2 - 1).f2 + elementsWithDistance.get(size / 2 + 1 - 1).f2) / 2;
		} else// for odd number of elements, '1' is deducted as index starts
				// from zero
			return elementsWithDistance.get(((size + 1) / 2) - 1).f2;
	}

	public static double calcluateQ1(List<Tuple3<Centroid, Tuple2<Integer, Point>, Double>> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {
			size = elementsWithDistance.size() / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1).f2 + elementsWithDistance.get(size / 2 + 1 - 1).f2) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1).f2;
			}
		} else {// for odd number of elements, '1' is deducted as index starts
				// from zero
			size = (elementsWithDistance.size() - 1) / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1).f2 + elementsWithDistance.get(size / 2 + 1 - 1).f2) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1).f2;
			}
		}
	}

	public static double calcluateQ3(List<Tuple3<Centroid, Tuple2<Integer, Point>, Double>> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {// size added to the end to count from median
							// element
			size = elementsWithDistance.size() / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1 + size).f2
						+ elementsWithDistance.get(size / 2 + 1 - 1 + size).f2) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1 + size).f2;
			}
		} else {// for odd number of elements, '1' is deducted as index starts
				// from zero
			// size+1 added to the end to count from after median element
			size = (elementsWithDistance.size() - 1) / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1 + size + 1).f2
						+ elementsWithDistance.get(size / 2 + 1 - 1 + size + 1).f2) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1 + size + 1).f2;
			}
		}
	}

	/**
	 * Get normalized values, the mininum and maximum is received for each
	 * attribute of the row that means the total number of min and maxium will
	 * be 2*dimension.
	 */
	public static Double[] getNormalizedValues(Double[] minMax, Double x[]) {
		Double z[] = new Double[x.length];
		// Arrays.sort(x);//commented this line as I don't remember why I had
		// used it.
		// as when sorted the min-max doesn't get calcuated for the actual value
		int i = 0;
		int j = 0;
		for (double v : x) {
			if (v >= minMax[j] && v <= minMax[j + 1]) {
				z[i] = (double) (v - minMax[j]) / (double) (minMax[j + 1] - minMax[j]);
			} else {
				// the point is an outlier
			}
			if (z[i] != null) {
				if (Double.isNaN(z[i])) {
					z[i] = 0.0;
				}
			}
			i++;
			j += 2;// for next minmax is after 2numbers
		}
		return z;
	}

	/**
	 * Get normalized values
	 */
	public static Grid getRoundDownValues(Double x[], Integer parameter) {
		Integer z[] = new Integer[x.length];
		String id = "";
		for (int i = 0; i < x.length; i++) {
			if (x[i] == null) {
				x[i] = 0.0;
			}
			z[i] = (int) (x[i] * parameter);
			id += z[i].toString();
		}
		return new Grid(id, z);
	}

	public static double euclideanDistance(double pt1[], double pt2[]) {
		double distance = 0;
		for (int i = 0; i < pt2.length; i++) {
			distance += (pt1[i] - pt2[i]) * (pt1[i] - pt2[i]);
		}
		return Math.sqrt(distance);
	}

	public static double[] getLowerAndUpperBound(List<Double> distanceList) {
		double bounds[] = new double[2];
		double q1 = 0;
		double q3 = 0;
		if (distanceList.size() > 1) {
			q1 = calculateQ1ForDistance(distanceList);
			q3 = calculateQ3ForDistance(distanceList);
		}
		// find IQR
		double iqr = q3 - q1;
		// multiplying iqr by 1.5 to find bounds is standard practice
		bounds[0] = q1 - 1.5 * iqr;
		bounds[1] = q3 + 1.5 * iqr;
		return bounds;
	}

	public static double calculateQ1ForDistance(List<Double> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for even number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {
			size = elementsWithDistance.size() / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1) + elementsWithDistance.get(size / 2 + 1 - 1)) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1);
			}
		} else {// for odd number of elements, '1' is deducted as index starts
				// from zero
			size = (elementsWithDistance.size() - 1) / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1) + elementsWithDistance.get(size / 2 + 1 - 1)) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1);
			}
		}
	}

	public static double calculateQ3ForDistance(List<Double> elementsWithDistance) {
		int size = elementsWithDistance.size();
		// for even number of elements, '1' is deducted as index starts from
		// zero
		if (size % 2 == 0) {// size added to the end to count from median
							// element
			size = elementsWithDistance.size() / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1 + size)
						+ elementsWithDistance.get(size / 2 + 1 - 1 + size)) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1 + size);
			}
		} else {// for odd number of elements, '1' is deducted as index starts
				// from zero
			// size+1 added to the end to count from after median element
			size = (elementsWithDistance.size() - 1) / 2;
			// for even no of elements
			if (size % 2 == 0) {
				return (elementsWithDistance.get(size / 2 - 1 + size + 1)
						+ elementsWithDistance.get(size / 2 + 1 - 1 + size + 1)) / 2;
			} else {// for odd no of elements
				return elementsWithDistance.get(((size + 1) / 2) - 1 + size + 1);
			}
		}
	}
}
