package com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.functions.FunctionAnnotation.ForwardedFields;
import org.apache.flink.api.java.operators.IterativeDataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;

import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.Centroid;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.ClusteredPoint;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.Point;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.SelectNearestCenter;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.Tokenizer;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.TupleCentroidConverter;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.TupleClusteredPointConverter;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.calculateDistanceToCenter;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.getPoints;

/**
 * @author subash
 * input arguments eg: file:///home/subash/Desktop/thesis/data/agata_batch_modified.csv 
 * file:///home/softwares/flink-1.0.0/kmeans/centers 
 * file:///home/softwares/flink-1.0.0/kmeans/result 4
 * */

public class KMediansOutlierDetection {

	private static int rowsCount = 0;

	public static void main(String[] args) throws Exception {

		if (!parseParameters(args)) {
			return;
		}

		// set up execution environment
		ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		// get input data
		DataSet<String> data = getTextDataSet(env);
		DataSet<Tuple2<Integer, Double[]>> newDataSet = data.flatMap(new Tokenizer());
		DataSet<Point> points = newDataSet.map(new getPoints());
		DataSet<Centroid> centroids = newDataSet.map(new TupleCentroidConverter()).first(10);
		IterativeDataSet<Centroid> loop = centroids.iterate(numIterations);
		DataSet<Tuple2<Integer, Point>> clusteredPoints = points
				// compute closest centroid for each point
				.map(new SelectNearestCenter()).withBroadcastSet(loop, "centroids");
		DataSet<ClusteredPoint> newClusteredPoints = clusteredPoints.map(new TupleClusteredPointConverter());

		DataSet<Centroid> newCentroids = newClusteredPoints.map(new calculateMedianCenters())
				.withBroadcastSet(centroids, "centroids").withBroadcastSet(clusteredPoints, "clusteredPoints")
				.groupBy(0).reduce(new GroupCentroids()).map(new CentroidConverter());

		DataSet<Centroid> finalCentroids = loop.closeWith(newCentroids);
		// recalculating the points where they belong to with respect to the
		// newly found out centroids by applying median method.
		clusteredPoints = points
				// assign points to final clusters
				.map(new SelectNearestCenter()).withBroadcastSet(finalCentroids, "centroids");

		// System.out.println("clusteredPoints-----------------------");
		newClusteredPoints = clusteredPoints.map(new TupleClusteredPointConverter());
		// System.out.println("newClusteredPoints-----------------------");
		DataSet<Tuple2<Point, Boolean>> distancePoints = newClusteredPoints.map(new calculateDistanceToCenter())
				.withBroadcastSet(finalCentroids, "centroids").withBroadcastSet(clusteredPoints, "clusteredPoints");
		// System.out.println("distancePoints-----------------------");
		// System.out.println("distancePointsPrinted---------------------------------");
		if (fileOutput) {
			distancePoints.writeAsCsv(outputPath, "\n", " ");
			// since file sinks are lazy, we trigger the execution explicitly
		} else {
			distancePoints.print();
		}
		env.execute("KMedians Outlier Detection");
	}

	/** Converts a {@code Tuple3<Integer, Centroid,Point>} into a Centroid. */
	public static final class CentroidConverter implements MapFunction<Tuple3<Integer, Centroid, Point>, Centroid> {

		@Override
		public Centroid map(Tuple3<Integer, Centroid, Point> t) throws Exception {
			return new Centroid(t.f0, t.f1);// t.f1 because this is the centroid
											// point
		}
	}

	/**
	 * returns new centroids;
	 */
	@ForwardedFields("0")
	public static final class GroupCentroids implements ReduceFunction<Tuple3<Integer, Centroid, Point>> {

		@Override
		public Tuple3<Integer, Centroid, Point> reduce(Tuple3<Integer, Centroid, Point> p0,
				Tuple3<Integer, Centroid, Point> p1) {
			return new Tuple3<Integer, Centroid, Point>(p0.f0, p0.f1, p0.f2);
		}
	}

	/** Determines the distance of center for a data point. */
	public static final class calculateMedianCenters
			extends RichMapFunction<ClusteredPoint, Tuple3<Integer, Centroid, Point>> {
		private Collection<Centroid> centroids;
		private Collection<Tuple2<Integer, Point>> clusteredPoints;

		/**
		 * Reads the centroid values from a broadcast variable into a
		 * collection.
		 */
		@Override
		public void open(Configuration parameters) throws Exception {
			this.centroids = getRuntimeContext().getBroadcastVariable("centroids");
			this.clusteredPoints = getRuntimeContext().getBroadcastVariable("clusteredPoints");
		}

		/**
		 * Takes clusteredPoint p, i.e. a single point as input. Perform
		 * operations to detect whether this point is an out-lier or not.
		 * Returns with a boolean value if this point is an out-lier or not.
		 * This method gets called parallel for each points until the completion
		 * of execution for all points. The result is parallel written to the
		 * file by each node cluster.
		 **/
		@Override
		public Tuple3<Integer, Centroid, Point> map(ClusteredPoint p) throws Exception {
			List<Tuple3<Centroid, Tuple2<Integer, Point>, Double>> elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<Integer, Point>, Double>>();
			Tuple3<Integer, Centroid, Point> returnElement = new Tuple3<Integer, Centroid, Point>();// true
			elementsWithDistance = new ArrayList<Tuple3<Centroid, Tuple2<Integer, Point>, Double>>();
			Tuple3<Centroid, Tuple2<Integer, Point>, Double> newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
			Centroid centroid = new Centroid();
			for (Tuple2<Integer, Point> e : clusteredPoints) {
				// compute distance
				if (e.f0 == p.id) {// comparing centroid ids
					newElement = new Tuple3<Centroid, Tuple2<Integer, Point>, Double>();
					for (Centroid cent : centroids) {
						if (e.f0 == cent.id) {
							centroid = cent;
						}
					}
					double distance = e.f1.euclideanDistance(centroid);
					newElement.setFields(centroid, e, distance);
					elementsWithDistance.add(newElement);
				}
			}
			// sorting the distances in ascending order
			Collections.sort(elementsWithDistance, new Tuple3Comparator());
			// finding q1 and q3 for even number of element counts;
			Tuple3<Centroid, Tuple2<Integer, Point>, Double> median = null;
			double q3 = 0;
			// only calculate if size is greater than equal to 2
			if (elementsWithDistance.size() >= 2) {
				median = BaseFunctions.calculateMedian(elementsWithDistance);
			}
			boolean elementSet = false;
			for (Tuple3<Centroid, Tuple2<Integer, Point>, Double> elementWithDistance : elementsWithDistance) {
				returnElement = new Tuple3<Integer, Centroid, Point>();
				if (Arrays.equals(p.pt, elementWithDistance.f1.f1.pt)) {// comparing
																		// values
					// old code, some mistake must be here
					// returnElement.setFields(elementWithDistance.f1.f0,
					// elementWithDistance.f0,
					// elementWithDistance.f1.f1);
					if (median != null) {
						returnElement.setFields(median.f0.id, median.f0, elementWithDistance.f1.f1);
					} else {
						returnElement.setFields(elementWithDistance.f1.f0, elementWithDistance.f0,
								elementWithDistance.f1.f1);
					}
					elementSet = true;
				}
				if (elementSet) {
					break;
				}
			}
			// emit a new record with the center id, the data point and distance
			// to the center.
			return new Tuple3<Integer, Centroid, Point>(returnElement.f0, returnElement.f1, p);
		}
	}

	private static DataSet<String> getTextDataSet(ExecutionEnvironment env) {
		if (fileOutput) {
			// read the text file from given input path
			return env.readTextFile(pointsPath);
		} else {
			// get default test text data
			return null;
		}
	}

	// ***********************************************
	// UTIL METHODS
	// ***********************************************
	private static boolean fileOutput = false;
	private static String pointsPath = null;
	private static String outputPath = null;
	private static int numIterations = 10;

	private static boolean parseParameters(String[] programArguments) {

		if (programArguments.length > 0) {
			// parse input arguments
			fileOutput = true;
			if (programArguments.length == 4) {
				pointsPath = programArguments[0];
				// centersPath = programArguments[1];
				outputPath = programArguments[2];
				numIterations = Integer.parseInt(programArguments[3]);
				// numCentroids = Integer.parseInt(programArguments[3]);
			} else {
				System.err.println("Usage: KMedians <points path> <centers path> <result path> <num iterations>");
				return false;
			}
		} else {
			System.out.println("Executing K-Medians example with default parameters and built-in default data.");
			System.out.println("  Provide parameters to read input data from files.");
			System.out.println("  See the documentation for the correct format of input files.");
			System.out.println("  We provide a data generator to create synthetic input files for this program.");
			System.out.println("  Usage: KMeans <points path> <centers path> <result path> <num iterations>");
		}
		return true;
	}
}
