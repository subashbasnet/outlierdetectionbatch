package com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.functions.FunctionAnnotation.ForwardedFields;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.util.Collector;

import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.GBODAgataBatch.Grid;
import com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch.KMeansOutlierDetection.Point;

/**
 * 
 * Input:
 * file:///home/softwares/flink-1.0.0/kmeans/dfki-artificial-3000-unsupervised-
 * ad.csv file:///home/subash/Desktop/result 15
 */

public class GBODUnsupervisedData {

	private static boolean fileOutput = false;
	private static String inputPath = null;
	private static String outputPath = null;
	private static int rowsCount = 0;

	public static void main(String[] args) throws Exception {
		if (!parseParameters(args)) {
			return;
		}
		// set up execution environment
		ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		// setting parallelism
		// env.setParallelism(1);
		// get input data
		DataSet<String> data = getTextDataSet(env);
		DataSet<AgataData> dataSet = data.flatMap(new Tokenizer());
		DataSet<Tuple2<String, Double[]>> newDataSet = dataSet.map(new ConvertAgataData());
		DataSet<Point> points = newDataSet.map(new getPoints());
		Double[] minMax = { -16.632014, 11.751998, -14.934255, 14.067577 };
		DataSet<Point> newPoints = points.map(new PointNormalized(minMax));
		DataSet<Tuple2<Point, Grid>> pointsWithGridCoordinates = newPoints
				.map(new RoundDownNewPoints(Integer.parseInt(args[2])));
		// 262169 is the number of elements in agatadata
		DataSet<Tuple3<Grid, Double, Long>> gridWithDensity = pointsWithGridCoordinates.map(new AddCountAppender())
				.groupBy(2).reduce(new GridPointsCount()).map(new RetrieveGridWithCount())
				.map(new CalculateGridDensity(3000));
		// gridWithDensity.print();262169
		DataSet<Tuple3<Grid, Long, Boolean>> finalGrid = gridWithDensity.map(new FindOutlierGrid(3000))
				.withBroadcastSet(gridWithDensity, "gridWithDensity");
		DataSet<Tuple2<Point, Boolean>> finalPoint = pointsWithGridCoordinates.map(new FindOutlierPoint())
				.withBroadcastSet(finalGrid, "finalGrid");
		finalPoint.writeAsCsv("file:///home/subash/Desktop/test", "\n", " ", FileSystem.WriteMode.NO_OVERWRITE);
		env.execute();
	}

	/**
	 * finding the outlier points
	 **/
	public static final class FindOutlierPoint extends RichMapFunction<Tuple2<Point, Grid>, Tuple2<Point, Boolean>> {
		private Collection<Tuple3<Grid, Long, Boolean>> finalGrid;

		/**
		 * Reads the centroid values from a broadcast variable into a
		 * collection.
		 */
		@Override
		public void open(Configuration parameters) throws Exception {
			this.finalGrid = getRuntimeContext().getBroadcastVariable("finalGrid");
		}

		@Override
		public Tuple2<Point, Boolean> map(Tuple2<Point, Grid> gd) throws Exception {

			for (Tuple3<Grid, Long, Boolean> e : finalGrid) {
				if (gd.f1.id.equals(e.f0.id)) {
					return new Tuple2<Point, Boolean>(gd.f0, e.f2);
				}
			}
			// this should never reach
			return new Tuple2<Point, Boolean>(gd.f0, false);
		}
	}

	/**
	 * finding the outlier grids
	 **/
	public static final class FindOutlierGrid
			extends RichMapFunction<Tuple3<Grid, Double, Long>, Tuple3<Grid, Long, Boolean>> {
		int totalPoints = 0;
		int gbod_count_limit = 0;
		private Collection<Tuple3<Grid, Double, Long>> gridWithDensity;

		/**
		 * Reads the centroid values from a broadcast variable into a
		 * collection.
		 */
		@Override
		public void open(Configuration parameters) throws Exception {
			this.gridWithDensity = getRuntimeContext().getBroadcastVariable("gridWithDensity");
		}

		public FindOutlierGrid(int totalPoints) {
			this.totalPoints = totalPoints;
		}

		@Override
		public Tuple3<Grid, Long, Boolean> map(Tuple3<Grid, Double, Long> gd) throws Exception {
			List<Tuple3<Grid, Double, Long>> orderedGridList = new ArrayList<Tuple3<Grid, Double, Long>>();
			for (Tuple3<Grid, Double, Long> e : gridWithDensity) {
				orderedGridList.add(e);
			}
			Collections.sort(orderedGridList, new GridCountComparator());
			// here limit = (total grid density/total number of grids(p^m))
			double limit = (totalPoints) * 0.025;
			gbod_count_limit = 0;
			for (Tuple3<Grid, Double, Long> e : orderedGridList) {
				gbod_count_limit += e.f2;
				if (gbod_count_limit <= limit) {
					if (gd.f0.id.equals(e.f0.id)) {
						// the grid is an outlier
						return new Tuple3<Grid, Long, Boolean>(gd.f0, gd.f2, true);
					}
				} else {
					// leave the loop after the number of elements is more than
					// 0.05*total elements.
					break;
				}
			}
			// grid is not an outlier
			return new Tuple3<Grid, Long, Boolean>(gd.f0, gd.f2, false);
		}
	}

	/**
	 * finding density of each grid
	 **/
	public static final class CalculateGridDensity
			extends RichMapFunction<Tuple2<Grid, Long>, Tuple3<Grid, Double, Long>> {

		int totalPoints = 0;

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public CalculateGridDensity(int totalPoints) {
			this.totalPoints = totalPoints;
		}

		@Override
		public Tuple3<Grid, Double, Long> map(Tuple2<Grid, Long> gd) throws Exception {

			return new Tuple3<Grid, Double, Long>(gd.f0, (double) gd.f1 / (double) totalPoints, gd.f1);
		}
	}

	/**
	 * Grid based
	 */
	public static final class RetrieveGridWithCount
			implements MapFunction<Tuple4<Point, Grid, String, Long>, Tuple2<Grid, Long>> {

		@Override
		public Tuple2<Grid, Long> map(Tuple4<Point, Grid, String, Long> t) throws Exception {
			return new Tuple2<Grid, Long>(t.f1, t.f3);
		}
	}

	/**
	 * counts the number of points in a grid
	 */
	@ForwardedFields("0")
	public static final class GridPointsCount implements ReduceFunction<Tuple4<Point, Grid, String, Long>> {

		@Override
		public Tuple4<Point, Grid, String, Long> reduce(Tuple4<Point, Grid, String, Long> val1,
				Tuple4<Point, Grid, String, Long> val2) {
			return new Tuple4<Point, Grid, String, Long>(val1.f0, val1.f1, val1.f2, val1.f3 + val2.f3);
		}
	}

	/** Appends a count variable to the tuple. */
	@ForwardedFields("f0;f1")
	public static final class AddCountAppender
			implements MapFunction<Tuple2<Point, Grid>, Tuple4<Point, Grid, String, Long>> {

		@Override
		public Tuple4<Point, Grid, String, Long> map(Tuple2<Point, Grid> t) {
			return new Tuple4<Point, Grid, String, Long>(t.f0, t.f1, t.f1.id, 1L);
		}
	}

	/**
	 * now let's multiply each value by p, the atrributes value will range from
	 * 0 to p. the return will be integer values so it'll correspond to the
	 * coordinate of the grid. i.e. the value says which grid the point belongs
	 * to.
	 **/
	public static final class RoundDownNewPoints extends RichMapFunction<Point, Tuple2<Point, Grid>> {

		int parameter = 0;

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public RoundDownNewPoints(int parameter) {
			this.parameter = parameter;
		}

		@Override
		public Tuple2<Point, Grid> map(Point pt) throws Exception {

			return new Tuple2<Point, Grid>(pt, BaseFunctions.getRoundDownValues(pt.pt, parameter));
		}
	}

	/**
	 * Normalizes points with values between 0 and 1
	 */
	public static final class PointNormalized extends RichMapFunction<Point, Point> {

		Double minMax[] = new Double[5];

		@Override
		public void open(Configuration parameters) throws Exception {
			super.open(parameters);
		}

		public PointNormalized(Double[] minMax) {
			this.minMax = minMax;
		}

		@Override
		public Point map(Point pt) throws Exception {
			return new Point(BaseFunctions.getNormalizedValues(minMax, pt.pt));
		}
	}

	/** converts Tuple2<Integer, double[]> to Point */
	// @ForwardedFields("0->id")
	public static final class getPoints implements MapFunction<Tuple2<String, Double[]>, Point> {

		@Override
		public Point map(Tuple2<String, Double[]> value) {
			return new Point(value.f1);
		}
	}

	/**
	 * Splits sentences into words as a user-defined FlatMapFunction. The
	 * function takes a line (String) and splits it into multiple pairs in the
	 * form of "(1, Double[])" ( {@code Tuple2<String, Double[]>}).
	 */
	public static final class Tokenizer implements FlatMapFunction<String, AgataData> {

		@Override
		public void flatMap(String value, Collector<AgataData> out) {
			AgataData agataData = new AgataData();
			String[] splitData = value.split("\\s*,\\s*", -1);
			Double[] agata = new Double[splitData.length];
			for (int i = 0; i < splitData.length; i++) {
				if (!(splitData[i] == null) && !(splitData[i].length() == 0)) {
					agata[i] = Double.parseDouble(splitData[i]);
				} else {
					agata[i] = 0.0;
				}
			}
			agataData.setId(rowsCount + 1 + "");
			agataData.setData(agata);
			out.collect(agataData);
			rowsCount++;
		}
	}

	/**
	 * converts agatadata to required format
	 */
	// @ForwardedFields("0->id")
	public static final class ConvertAgataData implements MapFunction<AgataData, Tuple2<String, Double[]>> {

		@Override
		public Tuple2<String, Double[]> map(AgataData value) {
			return new Tuple2<String, Double[]>(value.getId(), value.getData());
		}
	}

	/**
	 * converts agatadata to required format
	 */
	// @ForwardedFields("0->id")
	public static final class ConvertAgataDataTest implements MapFunction<Tuple2<String, Double[]>, Tuple1<String>> {

		@Override
		public Tuple1<String> map(Tuple2<String, Double[]> value) {
			return new Tuple1<String>(value.f0);
		}
	}

	private static boolean parseParameters(String[] programArguments) {

		if (programArguments.length > 0) {
			// parse input arguments
			fileOutput = true;
			if (programArguments.length == 3) {
				inputPath = programArguments[0];
				outputPath = programArguments[1];
			} else {
				System.err.println("Usage: GBOD <points path> <centers path> <result path> <num iterations>");
				return false;
			}
		} else {
			System.out.println("  Usage: GBOD <points path> <centers path> <result path> <num iterations>");
		}
		return true;
	}

	// Note: the issue is I need to remove the first row of the csv file i.e.
	// the header with row names
	// as it'll throw exception while converting to numbers later
	// I also need to know each column is either string or number before I read
	// the csv file.
	private static DataSet<String> getTextDataSet(ExecutionEnvironment env) {
		if (fileOutput) {
			// read the text file from given input path
			return env.readTextFile(inputPath);
		} else {
			// get default test text data
			return null;
		}
	}

}
