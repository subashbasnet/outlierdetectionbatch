package com.dfki.flink.kmeans.outlierdetection.OutlierDetectionBatch;

public class AgataData {
  
	private String id;
	private Double[] data;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double[] getData() {
		return data;
	}

	public void setData(Double[] data) {
		this.data = data;
	}

	public AgataData(){}
}
