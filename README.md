* Implements Outlier Detection Algorithm for Flink using Dataset functions. 

### How do I get set up? ###

* Clone the git repository to your local.
* Observe pom file to know about flink batch configurations and  dependencies.
* Maven clean build the project.
* Eg: to run GBODAgataMOdifiedData, provide input parameters of length 3. 
<file-data-input-path><file-output-path><p>
Eg:file:///home/softwares/flink-1.0.0/kmeans/dfki-artificial-3000-unsupervised-ad.csv file:///home/subash/Desktop/result 15
* The input data files can be found in data folder of the project. 
* Paste the file in your local location and adapt to the input parameters.

### Contribution guidelines ###
*Extend the codes for other outlier methods
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Subash Basnet
* Flink community